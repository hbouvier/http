package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	netHttp "net/http"
)

type BasicAuthentication struct {
	Name   string
	Secret string
}

type Client struct {
	url         string
	credentials *BasicAuthentication
	headers     map[string]string
}

func New(url string, credentials *BasicAuthentication, headers map[string]string) Client {
	if headers == nil {
		headers = map[string]string{}
	}
	Client := Client{url: url, credentials: credentials, headers: headers}
	return Client
}

func (this *Client) Get(url string, headers map[string]string, response interface{}) (*netHttp.Response, error) {
	return this.request("GET", url, headers, nil, response)
}

func (this *Client) Post(url string, headers map[string]string, body interface{}, response interface{}) (*netHttp.Response, error) {
	return this.request("POST", url, headers, body, response)
}

func (this *Client) Put(url string, headers map[string]string, body interface{}, response interface{}) (*netHttp.Response, error) {
	return this.request("PUT", url, headers, body, response)
}

func (this *Client) Delete(url string, headers map[string]string, body interface{}, response interface{}) (*netHttp.Response, error) {
	return this.request("DELETE", url, headers, body, response)
}

///////////////////////////////////////////////////////////////////////////////

func (this *Client) request(method string, url string, header map[string]string, body interface{}, response interface{}) (*netHttp.Response, error) {
	jsonBuffer, marshallingError := toJsonByteArray(body)
	if marshallingError != nil {
		return nil, NewMarshallingError(fmt.Sprintf("%s %s >> %v >> serialization %v", method, url, body, marshallingError), marshallingError)
	}
	rawContent, httpResponse, networkError := this.send(method, url, header, jsonBuffer)
	if networkError != nil {
		return httpResponse, NewNetworkError(fmt.Sprintf("%s %s >> network %v", method, url, networkError), networkError)
	}

	switch {
	case httpResponse.StatusCode >= 200 && httpResponse.StatusCode <= 299:
		if unmarshallingError := parseJsonByteArray(rawContent, response); unmarshallingError != nil {
			return httpResponse, NewUnmarshallingError(fmt.Sprintf("%s %s >> %v >> deserialization %v >> %v", method, url, body, rawContent, unmarshallingError), unmarshallingError)
		}
		return httpResponse, OK
	case httpResponse.StatusCode == 401:
		authenticationResponse := ""
		if unmarshallingError := parseJsonByteArray(rawContent, &authenticationResponse); unmarshallingError != nil {
			return httpResponse, NewAuthenticationError(fmt.Sprintf("%s %s >> Unauthorized (HTTP/401) response deserialization error %v", method, url, unmarshallingError))
		}
		return httpResponse, NewAuthenticationError(fmt.Sprintf("%s %s >> Unauthorized (HTTP/401) >> %s", method, url, authenticationResponse))
	case httpResponse.StatusCode == 403:
		forbiddenResponse := ""
		if unmarshallingError := parseJsonByteArray(rawContent, &forbiddenResponse); unmarshallingError != nil {
			return httpResponse, NewForbiddenError(fmt.Sprintf("%s %s >> Forbidden (HTTP/403) response deserialization error %v", method, url, unmarshallingError))
		}
		return httpResponse, NewForbiddenError(fmt.Sprintf("%s %s >> Forbidden (HTTP/403) >> %s", method, url, forbiddenResponse))
	case httpResponse.StatusCode == 404:
		notFoundResponse := ""
		if unmarshallingError := parseJsonByteArray(rawContent, &notFoundResponse); unmarshallingError != nil {
			return httpResponse, NewNotFoundError(fmt.Sprintf("%s %s >> Not found (HTTP/404) response deserialization error %v", method, url, unmarshallingError))
		}
		return httpResponse, NewNotFoundError(fmt.Sprintf("%s %s >> Not found (HTTP/404) >> %s", method, url, notFoundResponse))
	case httpResponse.StatusCode >= 500 && httpResponse.StatusCode <= 599:
		serverFailureResponse := ""
		if unmarshallingError := parseJsonByteArray(rawContent, &serverFailureResponse); unmarshallingError != nil {
			return httpResponse, NewServerFailureError(fmt.Sprintf("%s %s >> Not found (HTTP/%d) response deserialization error %v", method, url, httpResponse.StatusCode, unmarshallingError), httpResponse.StatusCode)
		}
		return httpResponse, NewServerFailureError(fmt.Sprintf("%s %s >> Not found (HTTP/%d) >> %s", method, url, httpResponse.StatusCode, serverFailureResponse), httpResponse.StatusCode)
	}
	unexpectedResponse := ""
	if unmarshallingError := parseJsonByteArray(rawContent, &unexpectedResponse); unmarshallingError != nil {
		return httpResponse, NewNotOkError(fmt.Sprintf("%s %s >> Unexpected HTTP/%d status code response deserialization error %v", method, url, httpResponse.StatusCode, unmarshallingError), httpResponse.StatusCode)
	}
	return httpResponse, NewNotOkError(fmt.Sprintf("%s %s >> Unexpected HTTP/%d status code >> %s", method, url, httpResponse.StatusCode, unexpectedResponse), httpResponse.StatusCode)
}
func toJsonByteArray(object interface{}) ([]byte, error) {
	var jsonBuffer []byte

	switch body := object.(type) {
	case string:
		jsonBuffer = []byte(body)
	case []byte:
		jsonBuffer = body
	default:
		var marshallingError error
		jsonBuffer, marshallingError = json.Marshal(object)
		if marshallingError != nil {
			return nil, marshallingError
		}
	}
	return jsonBuffer, nil
}
func (this *Client) send(method string, url string, headers map[string]string, body []byte) ([]byte, *netHttp.Response, error) {
	requestHeaders := mergeHeader(this.headers, headers)
	return sendImpl(method, this.url+url, requestHeaders, this.credentials, body)
}

var sendImpl = func(method string, url string, headers map[string]string, credentials *BasicAuthentication, body []byte) ([]byte, *netHttp.Response, error) {
	http := &netHttp.Client{}
	request, requestError := netHttp.NewRequest(method, url, bytes.NewReader(body))
	if requestError != nil {
		return nil, nil, requestError
	}
	for key, value := range headers {
		request.Header.Add(key, value)
	}
	if credentials != nil {
		request.SetBasicAuth(credentials.Name, credentials.Secret)
	}
	response, responseError := http.Do(request)
	if responseError != nil {
		return nil, nil, responseError
	}

	defer response.Body.Close()
	content, readError := ioutil.ReadAll(response.Body)
	if readError != nil {
		return nil, response, readError
	}
	return content, response, nil
}

func parseJsonByteArray(rawContent []byte, result interface{}) error {
	switch body := result.(type) {
	case *string:
		(*body) = string(rawContent)
	case *[]byte:
		(*body) = rawContent
	default:
		if unmarshallingError := json.Unmarshal(rawContent, &result); unmarshallingError != nil {
			return unmarshallingError
		}
	}
	return nil
}

func mergeHeader(baseHeader map[string]string, requestHeader map[string]string) map[string]string {
	header := make(map[string]string)
	for k, v := range baseHeader {
		header[k] = v
	}

	for k, v := range requestHeader {
		// If the value is EMPTY and the
		// header was part of the default
		// headers, REMOVE it otherwise,
		// Add the header with an empty
		// value.
		if v == "" {
			_, ok := header[k]
			if ok {
				delete(header, k)
			}
		} else {
			header[k] = v
		}
	}
	return header
}
