package http

type Forbidden struct {
	message string
}

func NewForbiddenError(message string) *Forbidden {
	return &Forbidden{message: message}
}

func (this *Forbidden) Error() string {
	return this.message
}

func IsForbiddenError(err interface{}) bool {
	_, ok := err.(*Forbidden)
	return ok
}
