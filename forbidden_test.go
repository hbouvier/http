package http

import (
	"errors"
	"fmt"
	"testing"
)

func TestForbiddenError(t *testing.T) {
	msg := "Forbidden"
	if err := NewForbiddenError(msg); err != nil {
		if !IsForbiddenError(err) {
			t.Fatalf("Expected error to be an Forbidden (%#v)", err)
		}
		if fmt.Sprintf("%s", err) != msg {
			t.Fatalf("Expected error message to be '%s' but was '%s' >> (%#v)", msg, fmt.Sprintf("%s", err), err)
		}
	} else {
		t.Fatalf("Expected an Forbidden error")
	}

	if IsForbiddenError(errors.New("a generic error")) {
		t.Fatalf("Expected IsForbiddenError to return false on a generic error")
	}
}
