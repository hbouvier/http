GOCC=go
# To Compile the linux version using docker simply invoke the makefile like this:
#
# make GOCC="docker run --rm -t -v ${GOPATH}:/go hbouvier/go-lang:1.5"
GITHUB=gitlab.com
GITUSERNAME=hbouvier
GITPROJECTNAME=httpclient

all: initialize dep fmt build test coverage

clean:
	-rm -rf coverage.out go.mod

initialize: go.mod

go.mod:
	echo ${GOCC} mod init ${GITHUB}/${GITUSERNAME}/${GITPROJECTNAME}

dep:
	@echo "OK"

build: fmt
	${GOCC} install

fmt: *.go
	${GOCC} fmt

test:
	${GOCC} test -v -cpu 4 -count 1 -coverprofile=coverage.out

coverage: test
	${GOCC} tool cover -html=coverage.out

